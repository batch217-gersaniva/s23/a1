// console.log("Hello World");


// 3. Create a trainer object using object literals.
// 4. Initialize/add the following trainer object properties:
// - Name (String)
// - Age (Number)
// - Pokemon (Array)
// - Friends (Object with Array values for properties)
// 5. Initialize/add the trainer object method named talk that prints out the message Pikachu! I choose you!
// 6. Access the trainer object properties using dot and square bracket notation.
// 7. Invoke/call the trainer talk object method.



let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "BBulbasaur"],
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"]
	},


	intro: function(){
		console.log(this.name);
	}


	// talk: function(){
	// 	console.log(this.pokemon[0] " ! " + " I choose you! ");
	// }

}
console.log(trainer);
console.log("Result of dot notation");
trainer.intro();


console.log("Result of square bracket notation");
trainer.pokemon[0, 1, 2, 3];


// console.log("Result of talk method");
// trainer.talk();








// let person = {
// 	name: "April",
// 	talk: function(){
// 		console.log("Hello my name is " + this.name);
// 		}
// }
// console.log(person);
// console.log("Result from object method");
// person.talk();    //Invocation/calling a function

// // Add a property with an object method/function
// person.walk = function(){
// 	console.log(this.name + " walked 25 steps forward " )
// }
// person.walk();























// let myPokemon = {
// 	name: "Pikachu",
// 	level: 3,
// 	health: 100,
// 	attack: 50,
// 	tackle: function(){
// 		console.log("This pokemon tackled taegetPokemn");
// 		console.log("targetPokemn's health is now reduced to _targetPokemonHealth_");
// 	},
// 	faint: function(){
// 		console.log("pokemon fainted");
// 	}
// }

// console.log(myPokemon);

// // 
// function Pokemon(name, level){
// 	// properties
// 	this.name = name;
// 	this.level = level;
// 	this.health = 2 * level; //32
// 	this.attack = level; //16

// 	//Methods/Function
// 	this.tackle = function(target){
// 		console.log(this.name + " tackled " + target.name);
// 		console.log("targetPokemn's health is now to _targetPokemonHealth_");
// 		this.faint = function(){
// 			console.log(this.name + "fainted");
// 	}
// 	}
// }

// let pikachu = new Pokemon("Pikachu", 16);
// let rattata = new Pokemon("Rattata", 8);

// pikachu.tackle(rattata);